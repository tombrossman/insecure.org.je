Have something to add?

For simple suggestions, feel free to try the contact page on the live website:
https://www.insecure.org.je/contact/.

For anything more complex, please join the project as a contributor, or fork it 
and send a merge request with your updates.

Here is a helpful guide to get you started:
https://git-scm.com/book/en/v2/GitHub-Contributing-to-a-Project