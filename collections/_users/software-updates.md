---
layout: hero-image
hero: users-hero
categories: users software-updates
title: Software Updates
description: Regularly updating your devices is one of the most important ways to keep your data secure.
image: updates.jpg
---

## Updates are important for everyone, every device

Soon after software updates are released, malware authors begin reverse-engineering them to discover how they work and which flaws they patch, then craft new exploits to attack users who are slow to update. In some cases new exploits are developed within days or even hours. This plan of attack works particularly well for malware authors, because many users ignore or delay updates.

It is true that updates occasionally change the programs you use, in unexpected ways which may cause temporary inconvenience, but it is always faster to update and adapt than it is to recover from an exploit or ransomware.

## When to update

Should you update whenever you see a pop-up window offering to update for you? No, this is a terrible idea. Dodgy websites do this as a way to trick users into installing malware. Instead, learn how to initiate software updates yourself, usually from the main menu or settings page of your devices.

  - [Updating Windows 10 devices](https://support.microsoft.com/en-us/help/4027667/windows-update-windows-10) (support.microsoft.com)
  - [Updating macOS devices](https://support.apple.com/en-us/HT201541) (support.apple.com)
  - [Updating iOS devices](https://support.apple.com/en-us/HT204204) (support.apple.com)

The truth is that most software updates are quick and effortless, and if your operating system offers to automatically download and apply updates for you, you really must enable this. The programs you use are pretty good about periodically saving and backing up data, and if your updater unexpectedly restarts your computer while you are working on something (looking at you, Windows...) programs should restore the previous state of any open files you were working on when you re-launch them.

## What about Android?

Updating your Android devices? Android is hugely popular and powers the vast majority of smartphones. Sadly, updates are more complicated for must users due to the large number of Android versions in use, the variety of devices available, and vendors' tendency to customise the user interface and preload unnecessary apps. In general, you should look amongst the various settings for an option "About phone", which will tell you what version of Android you run and the date of your last update.

Android updates come monthly, and if your device has not been updated within the last 30 days, you should assume that the manufacturer of your device has delayed or discontinued support, and consider it vulnerable. This includes most Android devices in use today. If you choose an Android smartphone, stick with only the latest models from manufacturers known for supplying prompt updates. This will require a little research on your part but it is well worth it.

If you purchase a new device directly from Google, such as their "Pixel" brand of smartphones, they guarantee monthly security updates for three years from the date of device launch (which is not necessarily the date you purchased it).

[Here is Google's help page explaining how to update their Pixel brand of Android smartphones](https://support.google.com/pixelphone/answer/4457705?hl=en).
