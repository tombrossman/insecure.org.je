---
layout: hero-image
hero: users-hero
categories: users vpn
title: VPNs
description: Use a virtual private network to encrypt your first digital 'hop' out onto the public internet.
image: vpn.jpg
---

## What is a VPN?

VPN means virtual private network, a network link that protects the connection between your device and the VPN service. These are especially helpful on dodgy WiFi hotspots or for corporate networks, because the link is encrypted and impervious to eavesdropping or tampering by others on your local network.

VPNs are also popular for accessing geo-restricted content because your web traffic appears to originate from the VPN server (which can be in another country) and not from your actual location.

## Recommended VPNs

**Commercial VPN Providers** - Shopping for a VPN is challenging, due to the questionable ethics of many so-called review websites which exist maily to refer potential customers to VPN providers in exchange for a fee. This is usually done using something called an 'affilliate link', which contains hidden redirects or extra tracking code indentifying the referrer. With this in mind, selecting a VPN is actually pretty simple once you exclude all the dodgy review sites and affilliate links.

There is one review site using affiliate links which is worth reading. [This Wirecutter review](https://thewirecutter.com/reviews/best-vpn-service/) is transparent about how they make selections, generate income, and it contains good advice from security experts.

One recommended commercial VPN provider is [Mullvad](https://www.mullvad.net/en/), a privacy-respecting service with high technical standards. Mullvad does not use affiliate links and you can purchase service using a variety of payment methods, including cash stuffed in an envelope (if you are truly paranoid about tracking).

Mullvad works with Windows, macOS, Linux, Android, and iOS and setup is simple. You can also install their client on routers capable of running DD-WRT, OpenWrt, pfSense, Asus Merlin, or Tomato (though installation on routers is more complicated). Technical users can also use [Mullvad via the new WireGuard](https://www.mullvad.net/en/guides/category/wireguard/) protocol, which offers superior performance.

**Self-hosted VPNs** - For technical users capable of administering their own server, try [Algo VPN](https://github.com/trailofbits/algo/blob/master/README.md) or try [this example tutorial for running OpenVPN on Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-set-up-an-openvpn-server-on-ubuntu-16-04).
