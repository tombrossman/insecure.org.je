---
layout: hero-image
hero: users-hero
categories: users password-managers
title: Password Managers
description: Use a password manager to generate secure and unique passwords for every account.
image: password-manager.jpg
---

## You only need to remember one good password

It is not possible to memorise unique and strong passwords for every account you use. As a result, many users go with short and weak passwords, and then compound their mistake by re-using them across multiple accounts. That means that once someone guesses your weak password, they can try it with the other accounts you use and compromise them as well. Password managers resolve this issue and encourage good "password hygiene".

You only need to remember the password to unlock your password manager, and from there you can manage unique passwords for every service, copying passwords when you need to enter them. Password managers also generate very secure passwords. You may choose how long to make your passwords, whether to include special punctuation characters, and whether to use both upper- and lower-case letters.

Some password managers can generate a string of dictionary words, which are easier to memorise. The example password...

 `fog afternoon sincere legume commute slacking`

 ...is actually a very strong password, and is superior to something like `3;oFai@tg~#"`. Why? Because password length is a far more important factor than the diversity of the character set you use. Back in 2012, dedicated password cracking machines were already capable of trying 350 billion passwords per second, which meant that every possible eight-character Windows password could be cracked in less than six hours. Windows security has improved since then, but so has password cracking.

## Password managers to try

The simplest password manager to use is your web browser. Most web browsers will offer to save your user-name and password when you log in to a website. Go ahead and try it if you are not already using a password manager.

Here are three other popular password managers, but do shop around and choose one not on this list if you prefer it.

- [Keepass](http://keepass.info/) - Free - Windows, macOS, Linux, Android, iOS
- [LastPass](https://www.lastpass.com/) - Free & Paid plans - Windows, macOS, Linux, Android, iOS
- [Dashlane](https://www.dashlane.com/) - Free & Paid plans - Windows, macOS, Android, iOS
- [1Password](https://1password.com/) - From $3USD/month - All platforms supported
