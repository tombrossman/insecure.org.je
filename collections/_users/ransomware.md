---
layout: hero-image
hero: users-hero
categories: users ransomware
title: Ransomware
description: Ransomware is malware that locks your computer and mobile devices or encrypts your files.
image: ransomware.jpg
---

## Protect Yourself

There are two important steps you can take to protect your devices against this kind of malware.

Firstly, keep all your devices up to date. Learn how to initiate a check for updates — don't wait for a popup message as these are spoofed by fraudsters to trick you into downloading malware. Check your device's system settings for an update option and always install updates without delay. This is your most effective defense against malware.

You should also keep regular backups of important files. If you do fall victim to ransomware, you can restore your files from your backup instead of paying the ransom. This usually means keeping your backup files on a seperate device such as a removable hard drive. A third-party backup service online is an excellent choice, as these offer automatic backups and your files are stored remotely.

The last important step is to install ad-blocking software on all your devices. Ad networks are the preferred method for distributing malware due to their wide reach and lack of accountability. If you are concerned about the financial impact of ad blocking on websites, try donating or purchasing a subscription to websites and services you use instead of viewing their adverts. This is much safer for you.

## Damage Control

Have your files or devices already been locked up by ransomware? You do not have many options at this point (unless you can restore everything from backups), however you can try uploading an encrypted file to the website [https://www.nomoreransom.org/](https://www.nomoreransom.org/) to test whether a key to unlock them is available. The website is a collaboration of Europol's European Cybercrime Centre and several legitimate security companies, who have developed decryption tools for many ransomware variants.

Lastly, remember that you should **never pay online criminals the ransom**. You have no guarantee they will keep their word and you risk losing both your files and the ransom you paid.
