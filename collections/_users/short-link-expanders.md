---
layout: hero-image
hero: users-hero
categories: users short-links
title: Short Links / Short URLs
description: Use a 'link expander' to test where a shortened link redirects to before you click on one.
image: link-expander.jpg
---

## What are Short Links?

URL shortening services provide 'short links' or short URLs, which are used to track visitors and redirect them to a destination which is hidden from the user until after they click it. These are useful for marketers who lack the technical skills to deploy safer click tracking methods, however fraudsters use this same trick to hide the real link - which leads to an attack site.

Short links also slow down your web browsing by adding long delays while the redirect services execute tracking scripts, collect data about your device, then finally send you to the destination website.

Fortunately these schemes are easy to circumvent using something called a **link expander**. Websites such as [CheckShortURL.com](https://checkshorturl.com/) can be used to reveal the destination site, and perform a cursory security scan before you visit. For an even simpler alternative you can try [wheredoesthislinkgo.com](http://www.wheredoesthislinkgo.com/).

Avoid clicking on third-party short links when you see them used online. Some social media services convert all links to short URLs but you can still see the destination when you hover your mouse pointer over the link on your desktop web browser. This is not possible on mobile however, and third-party shortened links such as bit.ly and goo.gl remain unsafe to use because you cannot trust where they link to without additional testing.

Best practice is to avoid shortened links, and if you have the occasional one you really need to see, test it with a link expander first.
