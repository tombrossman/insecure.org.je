---
layout: hero-image
hero: users-hero
categories: users website-scanners
title: Remote Website Scanners
description: Not sure about a particular website? Try one of these tools to scan it remotely.
image: website-scanners.jpg
---

If you want to visit a website but are not quite sure it is safe, you can use a third-party tool to test the site for malware and general trustworthiness before visiting. These tools are not 100% accurate, but they do catch many sites delivering malware. Two recommended sites to try are [sitecheck.sucuri.net](https://sitecheck.sucuri.net/) and [urlscan.io](https://urlscan.io/).

As a reminder, ad networks can deliver unique content tailored for each individual user. What this means is that a remote scan can come back clean but you are still at risk of malware when visiting the site because of how ad content is delivered. Best practice is to install and use an ad blocker, even for trusted and well-known websites.
