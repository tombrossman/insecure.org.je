---
layout: hero-image
hero: users-hero
categories: users website-scanners
title: Two-Factor Authentication
description: Combine something you have, something you are, and something you know, for improved security.
image: 2fa.jpg
---

Two-factor authentication is the single best way to secure any account and this scheme is widely supported. You are probably already using "2FA", as it is frequently written, when you pay someone with a bank card.

The concept of factors is simple. They are something you *have* (such as a bank card or mobile), something you *are* (such as your fingerprint), or something you *know* (such as a password or secret PIN code). Most importantly, 2FA protects you against a common scenario such as password theft. If your computer is compromised and your passwords stolen, the attacker is unable to log in to your accounts because they do not possess the second factor.

Setting it up for your online accounts is fairly simple and you have many options. In a typical use-case, you may purchase an inexpensive 2FA USB key, or download a mobile app which generates temporary codes, and use either option with your standard user-name/password combination. To see a list of websites and services that support 2FA, try [twofactorauth.org](https://twofactorauth.org/).

Again, two-factor authentication is the single best way to secure any account and this scheme is widely supported. Please start using it on all your accounts.
