---
layout: hero-image
hero: users-hero
categories: users ad-blocking
title: Ad Blocking
description: Blocking advertisements is now a basic security precaution which you should begin doing immediately.
image: ad-blocking.jpg
---

Ad blocking and filtering is a rapidly growing method of increasing security and privacy for internet users. Ad blocking among UK users was already at 22% in 2016, and continues to increase.

With minimal effort and using free tools, you can greatly reduce your chances of falling victim to malware and deliver huge speed gains during normal web browsing. In many cases, websites will load much faster due to the massive amount of bloat and tracking introduced by internet advertisements on web pages. You will also save bandwidth, which is important if your monthly plan's usage is limited.

If you visit 'free' websites that are ad-supported and have concerns about their financial viability, consider upgrading to a paid subscription (if offered) or make a donation to the site so that it may continue to operate.

## Malvertising

The term malvertising is a portmanteau of malicious + advertising. Online malvertising events number in the billions and are steadily increasing year after year. Due to the prevalence of adverts online, it is impossible for regular visitors to know which sites are safe and which contain dangerous adverts. When you visit a website which embeds ads from third-party networks, a complicated system quickly tries to determine the most relevant ad to display, and selects the ad from the highest bidder. This happens in a fraction of a second, using a variety of selectors such as where you are browsing from and what type of device you use. This makes it difficult to track malvertising because it is served intermittently.

Malvertising attacks have affected large, high-traffic websites such as BBC News, Yahoo.com, the New York Times, Spotify, and many more. It is critically important to use an ad-blocker as your first line of defense against these widespread attacks.

## Recommended Tools

**Desktop / Laptop**  
Install [uBlock Origin](https://github.com/gorhill/uBlock#installation). It runs on Chrome, Firefox, Safari, and Edge web browsers.

**Mobile**  
For Android, you can also use [uBlock Origin](https://github.com/gorhill/uBlock#installation). For iOS, try [Firefox Focus](https://itunes.apple.com/app/id1055677337) either as a standalone browser or [with Safari](https://support.mozilla.org/en-US/kb/focus).

**Routers**  
While this is more complicated, blocking adverts using your internet router is a great approach because all devices using your WiFi are automatically protected. If your router supports it, you can use a custom [hosts file](https://en.wikipedia.org/wiki/Hosts_%28file%29) for ad-serving domains. A more elegant approach is to use [Pi-hole](https://pi-hole.net/), a free and open-source ad blocker that you install on a Raspberry Pi, a spare Linux machine, or high-end routers.
