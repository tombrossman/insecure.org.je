---
layout: hero-image
hero: users-hero
categories: users mobile-devices
title: Mobile Devices
description: Enable a screen lock and device encryption to protect your data on mobile devices.
image: mobile-devices.jpg
---

Your mobile stores sensitive data and you should take a moment to ensure you are protecting that data. Nothing complicated, just a quick review and maybe a settings change to enable encryption. Sooner or later you will upgrade when your old one is lost, stolen, or becomes too slow.

## Enable automatic screen locking

Firstly, set a password/passcode or PIN to unlock your handset if you have not done so already. Even with phones that support fingerprint or facial recognition, you can set a password to require when switching them on or changing security settings.

## Enable device encryption

Next, enable encryption on your handset. All modern smartphones support this.

To check whether your **Android** handset is encrypted touch Settings > Security & location > Encryption & credentials and look for the message “Phone encrypted”. If you do not see this go ahead and enable it on that same screen. This could take a little while if you have a lot of data stored.

To check whether your **iOS** handset is encrypted touch Settings > Touch ID & Passcode and scroll down to look for the message “Data protection is enabled” This is enabled by default once you set a passcode.
