---
layout: hero-image
hero: users-hero
categories: users phishing
title: Phishing Scams
description: Learn how to identify and avoid 'phishing' scams, which are designed to fool you.
image: phishing.jpg
---

## What is Phishing?

Phishing is an attempt to obtain sensitive information such as usernames and passwords under a false pretext. There are many ways fraudsters try to fool you, and the thing to always remember is "who initiated contact?". It is the single most important indicator.

You can only be certain about who you are communicating with when you initiate contact. Never trust the other party if they contact you first, and never trust any links or contact information they supply. There are other ways to learn that information. For example, if you need to check on an urgent query from someone claiming to be your bank, you already have their contact information printed on your bank card and on previous bank statements. Same goes for any other accounts, contact info is on the statements.

## Known Phishing Tactics

**Spoofed Email Address:** The email address displayed can be spoofed if the domain owner has failed to take steps to authenticate their emails using simple modern countermeasures. Email sender authentication schemes such as SPF, DKIM, and DMARC are free and are widely deployed by responsible senders, but sadly some put their users at risk by not authenticating sent emails. Your email provider will automatically check authentication for you when present, and will block spoofed emails when fraudsters try to impersonate authenticated email senders. As an email recipient, your best defense is to avoid clicking links in emails asking you to log in to something, and use a trusted bookmark for the website instead.

**Spoofed Telephone Number:** The number displayed for the calling party can be spoofed and is not a trustworthy indicator of who is on the line. You can be sure you know who you are speaking with by hanging up and dialling a number you know is correct.

**Lookalike Domain Names**: Fraudsters cannot easily spoof the name of a website you visit, but they can use visually similar names to trick you. An example is bankofthe**vv**est.com, with two 'v' instead of one 'w' which the legitimate online banking domain bankofthe**w**est.com uses. Fraudsters can also easily add a legitimate domain name somewhere in the link such as bankofthewest.com.fraudsters.com or scam-site.com/something/something/bankofthewest.com. Your browser will highlight the actual domain name in darker text if you look at the address bar, though this is not always easy to spot. This is why you never trust the message you received - instead you should look up the real domain independently and use that instead.

**Spoofed Link Text:** This one is easy to spot by hovering your mouse pointer over a link and looking at the bottom of your browser window for the actual link destination. Mobile users can't do this of course, but here is an example: [bbc.co.uk](https://www.itv.com/ "ITV.com"). The link text on the page reads 'bbc.co.uk' but if you hover over or click on the link, you see it takes you to ITV instead of the BBC website. You can avoid this one by bookmarking important sites and using your bookmarks instead of clicking dodgy links.
