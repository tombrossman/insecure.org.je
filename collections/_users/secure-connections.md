---
layout: hero-image
hero: users-hero
categories: users secure-connection
title: Secure Websites
description: Look for the visual clues telling you whether your connection to a website is trustworthy.
image: secure-connections.jpg
---

Web browsers each use slightly different visual clues to let you know when your connection to a website is encrypted, but the one thing they all have in common is a closed padlock icon **in the address bar**.

<figure class="illustration">
  <img src="{{ site.baseurl }}/assets/images/advice/websites-padlock-icon.png" alt="" />
  <figcaption>Secure padlock icons in 3 popular web browsers</figcaption>
</figure>

It may be dark grey, or light green, or it might even include the name of the business operating the website against a green background. Despite the slight variations in appearance, the presense of a closed padlock icon in the address bar means the connection is secure. Do not be fooled by an image of a padlock on the web page itself, this is misleading and fraudsters often do this to trick you.

The image shown here illustrates a secure connection to the States of Jersey website **https**://www.gov.je with some popular web browsers. At the top is Firefox for Android, in the middle is Internet Explorer, and below that is Safari on an iPhone. All use a closed padlock icon in the address bar, seen here inside the green circles (added later for clarity). Padlock icons elsewhere are meaningless, always look for them in the address bar before entering personal data or passwords.

To learn more about your specific browser please visit their help pages which provide visual examples of what to look for.

  - [Chrome's help page explaining secure connections is here](https://support.google.com/chrome/answer/95617?hl=en)
  - [Firefox's help page explaining secure connections is here](https://support.mozilla.org/en-US/kb/how-do-i-tell-if-my-connection-is-secure)
  - [Safari's help page explaining secure connections is here](https://help.apple.com/safari/mac/9.0/#/sfri40697)
  - [Edge's help page explaining secure connections is here](https://support.microsoft.com/en-us/help/4027268/windows-how-to-know-whether-to-trust-a-website-in-microsoft-edge)

  Microsoft's help page for Internet Explorer is unknown at this time, apologies. We reached out to Microsoft for help locating this information in 2016 and have yet to receive a reply.
