---
description: PR agency with unencrypted client login page.
layout: posts-default
categories: websites
image: /assets/images/sites/direct-input.jpg
link:
  - http://www.directinput.je/cling/
notification: 28th August 2020
---

{% include page-sections/security-flaws/passwords.html %}
{% include page-sections/security-flaws/personal-info.html %}
