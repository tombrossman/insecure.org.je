---
title: "my|deposits Jersey"
description: Financial services company with insecure registration and login pages
layout: posts-default
categories: websites
image: /assets/images/sites/my-deposits-jersey.jpg
link:
  - http://crm.mydepositsjersey.je/register/
  - http://crm.mydepositsjersey.je/Login/Tenants
notification: 4th September 2018
---

Licensed by the government of Jersey to operate a Tenancy Deposit Scheme, this website has multiple insecure pages which collect sensitive personal info and passwords from tenants via an insecure connection.

Curiously, the login page for landlords requires encryption for logging in, however tenant login pages do not. This is probably just a misconfiguration of the server as encryption is available, but is only selectively enforced.

{% include page-sections/security-flaws/passwords.html %}
{% include page-sections/security-flaws/personal-info.html %}
