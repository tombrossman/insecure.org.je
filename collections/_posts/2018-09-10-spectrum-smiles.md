---
description: Dental practice with insecure appointment booking form
layout: posts-default
categories: websites
image: /assets/images/sites/spectrum-smiles.jpg
link: http://www.spectrumsmiles.co.uk/book-appointment
notification: 3nd September 2018
---

{% include page-sections/security-flaws/personal-info.html %}
