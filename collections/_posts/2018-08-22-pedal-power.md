---
description: Cycling e-commerce website registration and login via insecure connection
layout: posts-default
categories: websites
image: /assets/images/sites/5339-pedal-power.jpg
link: http://www.5339.co.uk/index.php?route=account/register
notification: 00st Month 2018
---

{% include page-sections/security-flaws/passwords.html %}
{% include page-sections/security-flaws/personal-info.html %}
