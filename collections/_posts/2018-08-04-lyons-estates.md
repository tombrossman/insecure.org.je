---
description: Estate agent website registration and tenant maintenance requests via insecure connection
layout: posts-default
categories: websites
image: /assets/images/sites/lyons-estates.jpg
link:
  - http://www.lyonsestates.co.uk/register.aspx
  - http://www.lyonsestates.co.uk/login.aspx
  - http://webutils.acquaintcrm.co.uk/maintenancerequests.aspx?siteprefix=LYON
notification: 4th August 2018
---

Personal, contact, and address details collected via unencrypted form. Also, a tenant maintenance request system collects personal info and photos via an unencrypted form.

{% include page-sections/security-flaws/passwords.html %}
{% include page-sections/security-flaws/personal-info.html %}
