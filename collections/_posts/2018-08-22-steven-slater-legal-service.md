---
description: Law firm using a contact form on an insecure web page.
layout: posts-default
categories: websites
image: /assets/images/sites/steven-slater-legal.jpg
link: http://www.slaterlaw.je/contact-us
notification: 22nd August 2018
---

{% include page-sections/security-flaws/personal-info.html %}
