---
description: Local travel agency with insecure booking form
layout: posts-default
categories: websites
image: /assets/images/sites/itravel.co.je.jpg
link: http://itravel.co.je/contact/holiday-booking-form-confirmation/
notification: 2nd September 2018
---

Local company "Independent Travel Limited" trading as I. Travel, whose holiday booking form collects passenger names, dates of birth, addresses, email, telephone numbers, and vehicle details over an insecure connection. No secure version of the page is available at time of posting.

{% include page-sections/security-flaws/personal-info.html %}
