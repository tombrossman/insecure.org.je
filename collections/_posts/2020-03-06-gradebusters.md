---
title: GradeBusters
description: Study & tutorial centre's registration and login pages are unencrypted
layout: posts-default
categories: websites
image: /assets/images/sites/gradebusters.jpg
link:
  - http://gradebusters.co.uk/login/
  - http://gradebusters.co.uk/register/
notification: 31st March 2020
---

{% include page-sections/security-flaws/passwords.html %}
{% include page-sections/security-flaws/personal-info.html %}
