---
description: Vehicle importer targeting Jersey residents
layout: posts-default
categories: websites
image: /assets/images/sites/exportautos.png
link:
  - http://www.exportautos.co.uk/find-me-a-car/
  - http://www.exportautos.co.uk/sell-your-car/
notification: 6th August 2019
---

Site asks for detailed information about vehicle and owner via an insecure connection by default.

{% include page-sections/security-flaws/personal-info.html %}
