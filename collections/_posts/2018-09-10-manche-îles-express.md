---
description: Local ferry service with insecure booking form
layout: posts-default
categories: websites
image: /assets/images/sites/manche-iles.jpg
link: http://www.manche-iles.com/en/booking/
notification: 2nd September 2018
---

Ferry service's online booking form collects passenger names, dates of birth, ID document numbers, and more over an insecure connection.

{% include page-sections/security-flaws/personal-info.html %}
