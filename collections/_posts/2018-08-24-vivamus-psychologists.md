---
description: Psychology practice with consultation bookings via insecure connection
layout: posts-default
categories: websites
image: /assets/images/sites/vivamus-psychologists.jpg
link: http://www.vivamuspsychologists.co.uk/
notification: 24th August 2018
---

{% include page-sections/security-flaws/personal-info.html %}
