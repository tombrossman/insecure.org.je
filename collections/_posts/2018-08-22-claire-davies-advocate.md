---
description: Advocate website with contact form served via insecure connection
layout: posts-default
categories: websites
image: /assets/images/sites/claire-davies-advocate.jpg
link: http://www.jerseyfamily.law/contact/
notification: 22nd August 2018
---

{% include page-sections/security-flaws/personal-info.html %}
