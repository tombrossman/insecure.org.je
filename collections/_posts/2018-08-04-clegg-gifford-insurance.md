---
description: Jersey insurance broker with household insurance form collecting property and personal info via insecure connection
layout: posts-default
categories: websites
image: /assets/images/sites/clegg-gifford.jpg
link: http://www.cgjersey.com/products_household_insurance_quote.php
notification: 4th August 2018
---

Site asks you to list all items in your home with valuations over £2000 and send that data to them, along with your address, over an unencrypted connection. This is a bad idea.

{% include page-sections/security-flaws/personal-info.html %}
