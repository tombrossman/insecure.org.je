---
description: Local electronics retailer with insecure registration and login forms
layout: posts-default
categories: websites
image: /assets/images/sites/fortuna-euronics.jpg
link:
  - http://www.fortunajersey.com/login
  - http://www.fortunajersey.com/register
notification: 30th January 2020
---

Personal, contact, and password details collected via unencrypted form.

{% include page-sections/security-flaws/passwords.html %}
{% include page-sections/security-flaws/personal-info.html %}
