---
description: A business broker collecting user data via an insecure form page
layout: posts-default
categories: websites
image: /assets/images/sites/lakey.jpg
link: http://lakeyoffshore.com/enquire/
notification:  27th June 2016
---

A business broker website promising confidentiality and collecting personal and business details via an insecure page.

The message above the form on the unencrypted page reads: "All information you provide will be treated in the strictest confidence." Site was redone in 2019, same pages still insecure.

{% include page-sections/security-flaws/personal-info.html %}
