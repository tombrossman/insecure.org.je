---
description: Local gas company allows contact info and meter readings to be sent via unencrypted connection.
layout: posts-default
categories: websites
image: /assets/images/sites/jsy-gas.jpg
link:
  - http://www.jsygas.com/online-services/submit-reading.php
  - http://www.jsygas.com/contact/index.php
notification: 22nd June 2015
---

{% include page-sections/security-flaws/personal-info.html %}
