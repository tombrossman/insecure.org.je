---
description: Local car hire company online reservations made over an insecure connection
layout: posts-default
categories: websites
image: /assets/images/sites/sovereign.jpg
link: http://carhire-jersey.com/book-now/
notification: 14th July 2015
---

Name, DOB, contact info, mobile number, driving license details, occupation, flight/ferry details, hotel info, all collected via insecure connection.

{% include page-sections/security-flaws/personal-info.html %}
