---
description: Car dealership and services with service booking and valuation forms collecting personal data via insecure web pages
layout: posts-default
categories: websites
image: /assets/images/sites/jacksons-cars.jpg
link:
  - http://www.jacksonsci.com/service-department/book-a-service/
  - http://www.jacksonsci.com/forms/value-my-vehicle/
notification: 4th August 2018
---

Premium auto dealership with multiple online forms collecting personal data and vehicle info via insecure web pages. Their [Privacy Policy](http://www.jacksonsci.com/legal/privacy-policy/) is actually pretty good overall and easy to read, however it states *"We will do everything within our power to keep your information safe, however data transmission via the Internet is not completely secure therefore we cannot guarantee the security of your data in transit."*

However it is not clear why data is sent unencrypted, with no effort made to protect customer data from interception, when adding encryption is so simple and inexpensive, and specifically recommended by Jersey's Information Commissioner.

For context, imagine a car dealer saying that driving is not 100% safe, and removing all airbags, seatbelts, and other safety devices from your car because they do not guarantee your safety. Time for a rethink.

{% include page-sections/security-flaws/personal-info.html %}
