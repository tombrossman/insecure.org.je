---
description: Watersports company with insecure activity & events booking pages
layout: posts-default
categories: websites
image: /assets/images/sites/jersey-sea-sport-centre.jpg
link:
  - http://www.jerseyseasport.com/groups-and-corporate-events.htm
  - http://www.jerseyseasport.com/prices-and-bookings.htm
  - http://www.romancart.com/cart.asp
notification: 16th August 2019
---

{% include page-sections/security-flaws/personal-info.html %}
