---
description: Law firm using a contact form on an insecure web page.
layout: posts-default
categories: websites
image: /assets/images/sites/andrew-begg-law.jpg
link: http://www.abcjersey.co.uk/
notification: 16th August 2019
---

{% include page-sections/security-flaws/personal-info.html %}
