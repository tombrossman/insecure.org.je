---
description: Gardening and Landscaping business with unencrypted ordering and login pages.
layout: posts-default
categories: websites
image: /assets/images/sites/garden-scene.jpg
link:
  - http://www.gardenscene.je/
  - http://www.gardenscene.je/cart/
notification: 8th May 2020
---

Strangely this business's [Data Privacy Policy](http://www.gardenscene.je/data-privacy-policy/) claims that *"All information
transferred between your internet browser and our website or third party applications are encrypted
using HTTPS protocol, using Digital Certificates with secure TLS Cyphers. This can be verified by the
appearance of the secure padlock symbol in the browser address bar."*

{% include page-sections/security-flaws/passwords.html %}
{% include page-sections/security-flaws/personal-info.html %}
