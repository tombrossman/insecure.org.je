---
description: Electronics repair service booking form served via insecure connection
layout: posts-default
categories: websites
image: /assets/images/sites/phone-doctor.jpg
link: http://www.phonedoctor.je/
notification: 22nd August 2018
---

{% include page-sections/security-flaws/personal-info.html %}
