---
title: M&S Jersey Customer Surveys
description: Customer feedback survey site collects personal data via an insecure web page
layout: posts-default
categories: websites
image: /assets/images/sites/marks-and-spencer-survey.jpg
link: http://www.tellmandsje.com/
notification: 4th August 2018
---

Third-party hosted customer survey site collects personal info to allow shoppers to enter into a monthly prize draw. This company, InMoment, claims in their [Privacy Policy](http://www.inmoment.com/privacy_policy/english/) that *"The Sites (including, but not limited to: websites, surveys, reporting applications, and data transfer applications), have commercially reasonable security measures consistent with industry standards in place to protect against the loss, misuse and interception of information by third parties."*.

It is unclear how InMotion could arrive at the conclusion that their security measures follow industry standards. Protecting user data from interception in transit by using encryption is a basic precaution that is widely understood.

{% include page-sections/security-flaws/personal-info.html %}
