---
description: Taxi service with unencrypted bookings page.
layout: posts-default
categories: websites
image: /assets/images/sites/jtda.jpg
link:
  - http://www.jerseytaxidriversassociation.co/book-a-taxi.html
notification: 3rd September 2022
---

{% include page-sections/security-flaws/passwords.html %}
{% include page-sections/security-flaws/personal-info.html %}
