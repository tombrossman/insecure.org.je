---
description: Travel agency with login on an insecure web page.
layout: posts-default
categories: websites
image: /assets/images/sites/estrela-travel.jpg
link: http://www.estrelatravel.co.uk/docs/
notification: 20th December 2019
---

{% include page-sections/security-flaws/passwords.html %}
{% include page-sections/security-flaws/personal-info.html %}
