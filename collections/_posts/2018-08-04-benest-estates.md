---
description: Estate agent website login and registration via insecure connection
layout: posts-default
categories: websites
image: /assets/images/sites/benest-estates.jpg
link: http://www.benestestates.com/Login.aspx
notification: 4th August 2018
---

{% include page-sections/security-flaws/passwords.html %}
{% include page-sections/security-flaws/personal-info.html %}
