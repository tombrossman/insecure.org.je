---
description: Medical website collecting sensitive patient data via insecure connection
layout: posts-default
categories: websites
image: /assets/images/sites/cleveland-clinic.jpg
link: http://clevelandclinic.co.uk/appointments
notification: 5th February 2019
---

Website for "Jersey’s second largest Medical Practice", with an insecure appointment booking page. The form asks for sensitive personal info including DOB and health insurance number.

{% include page-sections/security-flaws/personal-info.html %}
