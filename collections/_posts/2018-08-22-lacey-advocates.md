---
description: Advocate website with contact form served via insecure connection
layout: posts-default
categories: websites
image: /assets/images/sites/lacey-advocates.jpg
link: http://www.laceyadvocates.com/contact.html
notification: 22nd August 2018
---

{% include page-sections/security-flaws/personal-info.html %}
