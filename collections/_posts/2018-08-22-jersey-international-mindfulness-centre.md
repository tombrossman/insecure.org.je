---
description: Health services website with online bookings and shopping cart checkout via insecure connection
layout: posts-default
categories: websites
image: /assets/images/sites/jsy-intl-mindfulness.jpg
link:
  - http://www.jsyimc.co.uk/book-online/
  - http://www.jsyimc.co.uk/checkout/
notification: 22nd August 2018
---

{% include page-sections/security-flaws/passwords.html %}
{% include page-sections/security-flaws/personal-info.html %}
