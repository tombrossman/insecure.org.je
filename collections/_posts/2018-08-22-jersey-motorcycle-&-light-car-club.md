---
description: Racing club registration form served via insecure connection
layout: posts-default
categories: websites
image: /assets/images/sites/jerseymotorsport.com.jpg
link: http://www.jerseymotorsport.com/membership/joinus.aspx
notification: 22nd August 2018
---

{% include page-sections/security-flaws/personal-info.html %}
