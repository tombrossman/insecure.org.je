---
description: Maternity wear and children's clothing supplier's login pages are unencrypted
layout: posts-default
categories: websites
image: /assets/images/sites/la-lilou.jpg
link:
  - http://www.lalilou.co.uk/loginpage.aspx
notification: 28th March 2020
---

{% include page-sections/security-flaws/passwords.html %}
{% include page-sections/security-flaws/personal-info.html %}
