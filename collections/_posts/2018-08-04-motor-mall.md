---
description: Auto dealer and servicing with multiple forms collecting personal data on insecure web pages
layout: posts-default
categories: websites
image: /assets/images/sites/motor-mall.jpg
link:
  - http://www.motor-mall.co.uk/service-department/service-check-in/
  - http://www.motor-mall.co.uk/service-department/book-a-service/
  - http://www.motor-mall.co.uk/forms/value-my-vehicle/
notification: 4th August 2018
---

{% include page-sections/security-flaws/personal-info.html %}
