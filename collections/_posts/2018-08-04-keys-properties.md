---
description: Residential Lettings & Property Management tenant registration page via insecure connection
layout: posts-default
categories: websites
image: /assets/images/sites/keys-properties.jpg
link: http://www.keys.je/register.aspx?mode=1
notification: 4th August 2018
---

{% include page-sections/security-flaws/personal-info.html %}
