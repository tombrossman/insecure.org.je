---
description: Jersey Court Service's staff login pages are unencrypted
layout: posts-default
categories: websites
image: /assets/images/sites/tribunal-service.jpg
link:
  - http://tribunal.je/staff-area/loginlogout/
  - http://www.jerseyemploymenttribunal.org/staff-area/loginlogout/
notification: 14th February 2020
---

"Staff Area" login pages for the Tribunal Service are served via an unencrypted connection. There is not even a web server listening for requests on a secure port (:443).

{% include page-sections/security-flaws/passwords.html %}
{% include page-sections/security-flaws/personal-info.html %}
