---
description: Loan application details requested via an insecure connection
layout: posts-default
categories: websites
image: /assets/images/sites/acorn.jpg
link: http://www.acorn.je/apply-online/
notification: 27th June 2016
---

Finance company loan application requesting personal data from users via an insecure connection.

{% include page-sections/security-flaws/personal-info.html %}
