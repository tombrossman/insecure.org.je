---
description: Custom apparel company with client login via insecure connection
layout: posts-default
categories: websites
image: /assets/images/sites/logo.je.jpg
link: http://logo.je/
notification: 4th August 2018
---

{% include page-sections/security-flaws/passwords.html %}
{% include page-sections/security-flaws/personal-info.html %}
