---
description: Medical website collects personal info, medical info, and prescriptions via insecure web pages
layout: posts-default
categories: websites
image: /assets/images/sites/island-medical.jpg
link:
  - http://islandmedicalcentre.com/prescriptions/
  - http://islandmedicalcentre.com/contact-us/
notification: 1st July 2015
---

Website collects personal medical info including doctor, DOB, and prescription requests via unencrypted connection. An encrypted connection is available but not enforced.

Their [Privacy Policy](http://islandmedicalcentre.com/data-protection-and-patient-privacy-policy/) says *"Some services in the Practice provide the option to communicate with patients via email, SMS text or other electronic communications. Please be aware that the Practice cannot guarantee the security of this information whilst in transit, and by requesting this service you are accepting this risk."*

{% include page-sections/security-flaws/personal-info.html %}
