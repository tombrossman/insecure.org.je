---
description: School and police uniform supplier's login pages are unencrypted
layout: posts-default
categories: websites
image: /assets/images/sites/lyndale-sports.jpg
link:
  - http://www.lyndalesports.com/my-account/
  - http://www.http://www.lyndalesports.com/create-an-account/
notification: 28th March 2020
---

{% include page-sections/security-flaws/passwords.html %}
{% include page-sections/security-flaws/personal-info.html %}
