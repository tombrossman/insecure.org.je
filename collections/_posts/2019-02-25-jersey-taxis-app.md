---
description: Corporate Taxicard account login uses insecure connection
layout: posts-default
categories: websites
image: /assets/images/sites/jersey-taxis-app.jpg
link: http://www.jerseytaxis.je/account
notification: 5th February 2019
---


{% include page-sections/security-flaws/passwords.html %}
{% include page-sections/security-flaws/personal-info.html %}
