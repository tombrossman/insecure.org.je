---
description: Medical practice with insecure form collecting patient prescription info
layout: posts-default
categories: websites
image: /assets/images/sites/indigo-medical.jpg
link: http://www.indigomedical.je/prescriptions/
notification: 3nd September 2018
---

Name, DOB, contact info, and prescription drugs required are collected via form on insecure page

{% include page-sections/security-flaws/personal-info.html %}
