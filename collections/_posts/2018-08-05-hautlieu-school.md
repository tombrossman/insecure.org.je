---
description: School applications login via insecure connection
layout: posts-default
categories: websites
image: /assets/images/sites/hautlieu.jpg
link: http://www.apply.hautlieu.co.uk/
notification: 5th August 2018
---

{% include page-sections/security-flaws/passwords.html %}
{% include page-sections/security-flaws/personal-info.html %}
