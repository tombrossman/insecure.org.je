---
description: Property management website login and registration via insecure connection
layout: posts-default
categories: websites
image: /assets/images/sites/mulberry.jpg
link:
  - http://cip.mulberry.co.je/Signin
  - http://cip.mulberry.co.je/Property/Register
notification: 6th August 2018
---

HTTPS encryption for this site is available, however insecure requests are not upgraded to HTTPS as they should be.

{% include page-sections/security-flaws/passwords.html %}
{% include page-sections/security-flaws/personal-info.html %}
