---
description: Online business data backups access via insecure login page
layout: posts-default
categories: websites
image: /assets/images/sites/sigma-online-backup.jpg
link: http://www.sigmaci.com/BackupDR.aspx
notification: 4th August 2018
---

Online backups of business data are accessed via an insecure `<iframe>` hosted via an unencrypted web page connection. This is an exceptionally bad idea for sensitive business data and/or storage of customer personal data.

{% include page-sections/security-flaws/passwords.html %}
{% include page-sections/security-flaws/personal-info.html %}
