---
description: Law firm using a contact form on an insecure web page.
layout: posts-default
categories: websites
image: /assets/images/sites/lexstone-lawyers.jpg
link: http://www.lexstone.je/contact-details
notification: 16th August 2019
---

{% include page-sections/security-flaws/personal-info.html %}
