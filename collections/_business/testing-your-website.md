---
layout: hero-image
hero: business-hero
categories: business test
title: Testing the security of your own website
description: Simple tests you can perform, to evaluate the technical competence of your web host.
image: website-testing.jpg
---

## For Business Owners

So you paid someone to build you a website and it's done, now what?

You operate the business and that is your area of expertise, so you know how important it is to check the quality of the product. Here are some online tools from reputable organisations, which you can use to independently verify the site and web hosting you paid for are properly configured. You will also be better informed, and prepared to discuss any defects found (or identify someone more competent if it comes to that).

Mind that security is an ongoing process, so even if your site tests well today you should re-test at regular intervals. These tests are free and only take a minute, do them every six months or so and you are all set.

**Sucuri SiteCheck**  
Tests for known malware, viruses, blacklisting status, website errors, out-of-date software, and malicious code. Testing is free, with a paid monitoring service option.  
[https://sitecheck.sucuri.net/](https://sitecheck.sucuri.net/)

**urlscan.io**  
Tests domains and IPs contacted, the resources requested from those domains, as well as additional technical information about the page itself. Targeted at more advanced users.  
[https://urlscan.io/](https://urlscan.io/)

**Mozilla Observatory**  
Very comprehensive testing of your website and web server, with links to security documentation and extra third-party tests. If you only use one test, this is the one to choose.  
[https://observatory.mozilla.org/](https://observatory.mozilla.org/)


## For Developers

The above links are just as important for developers, plus a couple more suggestions for advanced users below.

**Content Security Policy Reference**  
Set HTTP headers to restrict how page resources load. If your current policy includes `unsafe-*` script source directives, this mostly defeats the purpose of having a CSP and you may need to source (or rewrite) better JavaScript. See also the [Laboratory CSP](https://addons.mozilla.org/en-US/firefox/addon/laboratory-by-mozilla/) Firefox add-on which can be used to generate your first policy by activating the add-on and browsing your website.  
[https://content-security-policy.com/](https://content-security-policy.com/)

**HSTS Preload List Submission**  
Get your clients switched over to HTTPS, then get their website hard-coded into web browser's HTTP Strict Transport Security preload lists. Make it impossible for end users to browse to your website via an insecure connection.  
[https://hstspreload.org/](https://hstspreload.org/)

**Dataskydd.net**  
Swedish NGO focusing on IT security, privacy and consumer rights. Online scanner to check that your site is privacy friendly, with resources to explain how to make it more so. Available in multiple languages.  
[https://webbkoll.dataskydd.net/en](https://webbkoll.dataskydd.net/en)
