---
layout: hero-image
hero: business-hero
categories: business https
title: HTTPS Encryption
description: Encryption protects the data exchanged between your website and its users.
image: secure-connections.jpg
---

## For Business Owners

When you enable HTTPS encryption for your website, you are protecting both your site and its users. Adding HTTPS is simple and free certificates are widely available. Ask whoever manages your website to switch you over to HTTPS, and if they refuse, or if they quote you more than a token amount to make the changes required, find someone else to manage your website.

<figure class="illustration">
  <img src="{{ site.baseurl }}/assets/images/advice/under-30-id-fraud.svg" alt="Graph showing UK ID theft victim numbers doubling between 2010 and 2015" />
  <figcaption>Under-30 UK Identity Fraud Victims, 2010 - 2015 | Source: <a href="http://home.bt.com/lifestyle/money/money-tips/coffee-shop-customers-shocked-by-like-stunt-in-cifas-data-to-go-video-11364071638280">BT</a></figcaption>
</figure>

Website traffic is unencrypted by default, and this means everyone using a shared network such as the free WiFi at a café, or a local network at an office, can read and collect everyone else's unencrypted traffic. Think of WiFi as a speaker-phone. Unless you are speaking in code, everyone within listening distance can hear both sides of your conversation. HTTPS encryption fixes this problem.

Encryption provides three security benefits: Confidentiality, Integrity and Authentication. What this means is that when a software client (such as a web browser) uses encryption to protect data that is transmitted to a server (such as a web site), encryption protects that data from interception by third parties (confidentiality), it ensures that the client and server will know if a third party has tampered with the data as it is transmitted between them (integrity), and can permit the client and server to be confident that they are talking to each other and not an imposter (authentication).

## For Developers

Want to quickly add HTTPS using [Let's Encrypt's Certbot](https://certbot.eff.org/) client? There are numerous applications and methods you can use, depending on your hosting set up, but for a typical LAMP stack running Ubuntu...

`add-apt-repository ppa:certbot/certbot && apt update && apt -y install python-certbot-apache`

...adds their PPA and installs the Apache HTTP Server plugin, then...

`certbot --apache -d example.com -d www.example.com`

...gets your certificates and guides you through the issuance process in a minute or two. Yes, it is literally that simple (not to mention free, though please do consider donating to the Certbot team if you find it useful!).

If you are on shared hosting or some other type of managed hosting with limited control, [here is a regularly updated list of companies that provide Let's Encrypt support](https://community.letsencrypt.org/t/web-hosting-who-support-lets-encrypt/6920). Or if you want to geek out and customise your HTTPS set up, try this excellent [SSL configuration generator from Mozilla](https://mozilla.github.io/server-side-tls/ssl-config-generator/).

For a static website, another free and simple way to switch to HTTPS is to use AWS' CloudFront. Create a CloudFront distribution and enable your domain as an "Alternate Domain Name", connect it to your website, then follow [their documentation to request a public certificate using the AWS Certificate Manager](https://docs.aws.amazon.com/acm/latest/userguide/gs-acm-request-public.html). This is even easier than Certbot and AWS will maintain and rotate certificates for you automatically. If you use AWS' Route 53 DNS you can have the necessary record sets automatically added for convenience.
