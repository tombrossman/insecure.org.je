---
layout: hero-image
hero: business-hero
categories: business email
title: Email Authentication
description: Digitally sign outgoing emails from your domain, to prevent spoofing and improve deliverability.
image: authenticity.jpg
---

## For Business Owners

Fraudsters can easy spoof the "from" address in emails, but you can block this by using digital authentication, which all responsible email providers support. If you use a third-party email provider such as Google or Microsoft to host business email at your domain, they support email authentication by default. You just need to publish the provided records to your DNS.

If you find that your emails to clients are filtered as spam or do not get through, this is likely due to a lack of email authentication on your part. You should make this a priority as it costs nothing to set up and does not require any ongoing maintenance.

If you don't understand what that means, you can try a simple test using the website [mail-tester.com](https://www.mail-tester.com/). They will check the 'spamminess' of your email for you and provide feedback.

## For Developers

Enable SPF, DKIM, and DMARC for every domain and subdomain using email. No exceptions.

A valid SPF record looks like this example: `"v=spf1 include:spf.messagingengine.com ?all"`

A valid DKIM record looks like this example: `"v=DKIM1; k=rsa; p=MIJJ74dHgWkqhkiG9w...(continues...)"`

A valid DMARC record looks like this example: `"v=DMARC1; p=none; sp=none; fo=1; ri=3600; ruf=mailto:test@example.com"`

Major email providers support SPF and DKIM authentication and you should refer to their documentation for specific records to publish, usually as DNS TXT records. You will need to collect a list of authorised sender domains (such as google.com if using G Suite, mailchimp.com for newsletters, etc...) for the SPF records, and either a domain or public encryption key for DKIM records.

DMARC you can deploy yourself, after SPF and DKIM are tested and working. Try an online record generator such as these from [dmarcian.com](https://dmarcian.com/record-tools/) or [Global Cyber Alliance](https://dmarc.globalcyberalliance.org/). If this is your first time setting it up, note that you can start out with a very permissive policy which only reports problems, without actually blocking anything. This is especially helpful if the client is disorganised and does not have complete records for all authorised email senders. Third-party marketing and survey services often get overlooked, and they all require published SPF and DKIM records before DMARC can be used effectively.

Once everything is tested and working, you can switch to a very strict policy instructing recipients to silently discard all unauthenticated email from your client's domain, using this example policy: `v=DMARC1; p=reject; adkim=s; aspf=s;`.
