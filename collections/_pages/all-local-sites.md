---
layout: posts-all
hero: local-sites-hero
permalink: /websites/index.html
title: All Jersey Websites Requiring Extra Caution
description: At time of posting, websites listed here require extra caution due to security and/or privacy issues.
---

{% for post in site.posts %}
<div class="padded">
  <article class="card">
    <a href="{{ post.url | prepend: site.baseurl }}" class="card-link">
      <img src="{{ post.image }}" alt="" />
      <footer>
        <h4>{{ post.title }}</h4>
        <p>Added: {{ post.date | date: "%b %Y" }}</p>
      </footer>
    </a>
  </article>
</div>
{% endfor %}
