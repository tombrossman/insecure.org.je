---
layout: posts-all
hero: users-hero
permalink: /users/index.html
title: Online safety and privacy resources for users
description: On this page is some general guidance for users, organised by subject. Click on any section below to learn more.
---

{% for item in site.users %}
<div class="padded">
  <article class="card">
    <a href="{{ item.url | prepend: site.baseurl }}" class="card-link">
      <img srcset="{{ site.baseurl }}/assets/images/advice/{{ item.image }}, {{ site.baseurl }}/assets/images/advice/2x-{{ item.image }} 2x" src="{{ site.baseurl }}/assets/images/advice/{{ item.image }}" alt="" />
      <footer>
        <h4>{{ item.title }}</h4>
        <p>{{ item.description }}</p>
      </footer>
    </a>
  </article>
</div>
{% endfor %}
