---
layout: posts-all
hero: business-hero
permalink: /business/index.html
title: Information and  resources for Jersey businesses
description: Below is some general guidance organised by subject. Click on any section to learn more.
---

{% for item in site.business %}
<div class="padded">
  <article class="card">
    <a href="{{ item.url | prepend: site.baseurl }}" class="card-link">
      <img src="{{ site.baseurl }}/assets/images/advice/{{ item.image }}" srcset="{{ site.baseurl }}/assets/images/advice/{{ item.image }}, {{ site.baseurl }}/assets/images/advice/2x-{{ item.image }} 2x" alt="" />
      <footer>
        <h4>{{ item.title }}</h4>
        <p>{{ item.description }}</p>
      </footer>
    </a>
  </article>
</div>
{% endfor %}
